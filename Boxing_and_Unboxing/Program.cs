﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boxing_and_Unboxing
{
    class Program
    {
        static void Main(string[] args)
        {
            //creating two variables and assigning them some values

            int number_one = 10;
            int number_two = 20;

            //boxing both of them 
            // by doing so I can send the two variables (which are both value types) as objects
            object one_o = number_one;
            object two_o = number_two;

            //now calling the add method
            int temp_result = add_two_objects(one_o, two_o);
            hello();
        }

        public void hello()
        {

        }
        //note here that since the parameters are objects, the method has to be static
        //or you will run into an error stating the same.
        public int add_two_objects(object object_one,object object_two)
        {
            //a note here.
            //boxing does not 'remember' which type the original type was
            //that means, the compiler itself will not check if you are doing the unboxing
            //properly
            //here the onus is entirely on you to do proper unboxing to the original type

            //suppose you do incorrect unboxing, there wont be any error during compile time
            //the compiler assumes you know what you are doing with the whole boxing/unboxing
            //process
            //however, run time errors will happen

            //that means, this is one of those places where you really want to use exception handling
            //even for routine boxing/unboxing
            
            //unboxing the first object into the int variable
            int first_number = (int)object_one;
            //unboxing the second object into the int variable
            int second_number = (int)object_two;

            //adding the now unboxed integer numbers
            int result_of_adding_two_numbers = first_number + second_number;

            //returnign the results
            return (result_of_adding_two_numbers);
        }
    }
}
